import {
    USER_LOGIN_REQUEST,
    USER_LOGIN_SUCCESS,
    USER_LOGIN_FAIL,
    USER_LOGOUT,

    USER_REGISTER_REQUEST,
    USER_REGISTER_SUCCESS,
    USER_REGISTER_FAIL,

    USER_ACCOUNT_REQUEST,
    USER_ACCOUNT_SUCCESS,
    USER_ACCOUNT_FAIL,
    USER_ACCOUNT_RESET,

    UPDATE_USER_ACCOUNT_REQUEST,
    UPDATE_USER_ACCOUNT_SUCCESS,
    UPDATE_USER_ACCOUNT_FAIL,
    UPDATE_USER_ACCOUNT_RESET,

} from '../constants/index'


// login reducer
export const userLoginReducer = (state = {}, action) => {
    switch(action.type) {
        case USER_LOGIN_REQUEST:
            return {
                loading: true
            }
            
        case USER_LOGIN_SUCCESS:
            return {
                ...state,
                loading: false,
                userInfo: action.payload
            }

        case USER_LOGIN_FAIL:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
            
        case USER_LOGOUT:
            return {}
        
        default:
            return state
    }
}


// register reducer
export const userRegisterReducer = (state = {}, action) => {
    switch(action.type) {
        case USER_REGISTER_REQUEST:
            return {
                loading: true
            }

        case USER_REGISTER_SUCCESS:
            return {
                ...state,
                loading: false,
                userInfo: action.payload
            }

        case USER_REGISTER_FAIL:
            return {
                ...state,
                loading: false,
                error: action.paylaod
            }

        case USER_LOGOUT:
            return {}

        default:
            return state
    }
}

// user account
export const getAccountReducer = (state = { user: {} }, action) => {
    switch (action.type) {
        case USER_ACCOUNT_REQUEST:
            return {
                loading: true,
                user: {}
            }
        case USER_ACCOUNT_SUCCESS:
            return {
                ...state,
                loading: false,
                user: action.payload
            }
        case USER_ACCOUNT_FAIL:
            return {
                loading: false,
                error: action.payload
            }
        case USER_ACCOUNT_RESET:
            return {}
        default:
            return state
    }
}

// update user details
export const updateAccountReducer = (state = {}, action) => {
    switch (action.type) {
        case UPDATE_USER_ACCOUNT_REQUEST:
            return {
                loading: true
            }
        case UPDATE_USER_ACCOUNT_SUCCESS:
            return {
                ...state,
                loading: false,
                success: true,
                userInfo: action.payload
            }
        case UPDATE_USER_ACCOUNT_FAIL:
            return {
                loading: false,
                error: action.payload
            }
        case UPDATE_USER_ACCOUNT_RESET:
            return {}

        default:
            return state
    }
}
