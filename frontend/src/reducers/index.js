import { combineReducers } from 'redux'
import { userLoginReducer, userRegisterReducer, getAccountReducer, updateAccountReducer } from './userReducers'

const allReducers = combineReducers({
    userLoginReducer,
    userRegisterReducer,
    getAccountReducer,
    updateAccountReducer,
})

export default allReducers