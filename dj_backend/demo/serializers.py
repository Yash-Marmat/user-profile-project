from rest_framework import serializers
from rest_framework_simplejwt.tokens import RefreshToken
from .models import CustomUser


class UserSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField(read_only=True) 

    class Meta:
        model = CustomUser
        fields = ['id', 'username', 'email', 'name', 'dob', 'phone_number']

    def get_name(self, obj):
        name = obj.first_name
        return name

    def get_lastName(self, obj):
        lastName = obj.last_name
        return lastName

class UserSerializerWithToken(UserSerializer):
    token = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = CustomUser
        fields = ['id', 'username', 'email', 'name', 'dob', 'phone_number', 'token']

    def get_token(self, obj):
        token = RefreshToken.for_user(obj)
        return str(token.access_token)
